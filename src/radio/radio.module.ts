import { Module } from '@nestjs/common';
import { RadioController } from './radio.controller';
import { radioLibraryProvider } from './radio-library.provider';
import { RadioService } from './radio.service';

@Module({
  controllers: [RadioController],
  providers: [radioLibraryProvider, RadioService],
})
export class RadioModule {}
