export interface ISong {
  name: string;
  id: number;
  duration: number;
}

export interface IArtist {
  name: string;
  Song: ISong[];
}
