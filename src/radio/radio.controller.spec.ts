import { Test, TestingModule } from '@nestjs/testing';
import { RadioController } from './radio.controller';
import { RadioService } from './radio.service';

describe('Radio Controller', () => {
  let controller: RadioController;
  let service: any;

  class MockRadioService {
    getPlaylist() {
      return ['song1', 'song2'];
    }
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RadioController],
      providers: [
        {
          provide: RadioService,
          useClass: MockRadioService,
        },
      ],
    }).compile();

    controller = module.get<RadioController>(RadioController);
    service = module.get<RadioService>(RadioService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('check getPlaylist method', () => {
    const result = controller.getPlaylist(2);
    expect(result).toStrictEqual(['song1', 'song2']);
  });
});
