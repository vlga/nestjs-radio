import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { RadioService } from './radio.service';

@Controller('radio')
export class RadioController {
  constructor(private readonly radioService: RadioService) {}

  @Get('playlist/:limit')
  getPlaylist(@Param('limit', new ParseIntPipe()) limit: number) {
    return this.radioService.getPlaylist(limit);
  }
}
