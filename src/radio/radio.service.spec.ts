import { Test, TestingModule } from '@nestjs/testing';
import { RadioService } from './radio.service';
import { RADIO_LIB } from './radio-library.provider';

describe('RadioService', () => {
  let service: RadioService;

  const mockLib = {
    firstCharacterMap: {
      a: [{ name: 'ab' }],
      b: [{ name: 'ba' }],
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RadioService,
        {
          provide: RADIO_LIB,
          useValue: mockLib,
        },
      ],
    }).compile();

    service = module.get<RadioService>(RadioService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('getRandomArrayElement should take random element from array', () => {
    const dummyArray = [1, 2, 3];
    const element = RadioService.getRandomArrayElement(dummyArray);
    expect(dummyArray.includes(element)).toBeTruthy();
  });

  it('should return random song', () => {
    const song = (service as any).getRandomSong();
    expect(typeof song).toBe('string');
  });

  it('should generate playlist', () => {
    const generator = (service as any).playListGenerator('ab');
    const song = generator.next();
    expect(song.value).toBe('ba');
    const nextSong = generator.next();
    expect(nextSong.value).toBe('ab');
  });

  it('should generate null values if there is no corresponding songs', () => {
    (service as any).radioLib = {
      firstCharacterMap: {
        a: [{ name: 'ab' }],
        b: [{ name: 'bc' }],
      },
    };
    const generator = (service as any).playListGenerator('ab');
    const song = generator.next();
    expect(song.value).toBe('bc');
    const nextSong = generator.next();
    expect(nextSong.value).toBeNull();
  });

  it('should generate list of songs according to limit', () => {
    const list2 = service.getPlaylist(2);
    expect(list2.length).toBe(2);

    const list1 = service.getPlaylist(1);
    expect(list1.length).toBe(1);

    const list3 = service.getPlaylist(3);
    expect(list3.length).toBe(3);

    (service as any).radioLib = {
      firstCharacterMap: {
        a: [{ name: 'ab' }],
        b: [{ name: 'bc' }],
      },
    };

    const list3limited = service.getPlaylist(3);
    expect(list3limited.length).toBeLessThanOrEqual(2);
  });
});
