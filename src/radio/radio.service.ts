import { Injectable, Inject } from '@nestjs/common';
import { RADIO_LIB } from './radio-library.provider';
import { ISongsMap } from './character-map.interface';

@Injectable()
export class RadioService {
  constructor(@Inject(RADIO_LIB) private readonly radioLib: ISongsMap) {}

  getPlaylist(limit: number): string[] {
    const songsList = [];
    let curSong: string | null = this.getRandomSong();
    songsList.push(curSong);
    const playlistGenerator = this.playListGenerator(curSong);

    for (let i = 0; i <= limit - 2 && curSong; i++) {
      curSong = playlistGenerator.next().value;
      if (curSong) {
        songsList.push(curSong);
      } else {
        break;
      }
    }

    return songsList;
  }

  private getRandomSong(): string {
    const randomCharacter = RadioService.getRandomArrayElement(
      Object.keys(this.radioLib.firstCharacterMap),
    );
    return RadioService.getRandomArrayElement(
      this.radioLib.firstCharacterMap[randomCharacter],
    ).name;
  }

  private *playListGenerator(startSongName: string): Generator<string | null> {
    let curCharacter = startSongName[startSongName.length - 1];
    while (true) {
      const songsList = this.radioLib.firstCharacterMap[curCharacter];
      if (!songsList || !Array.isArray(songsList)) yield null;
      const songName = RadioService.getRandomArrayElement(songsList).name;
      curCharacter = songName[songName.length - 1];
      yield songName;
    }
  }

  static getRandomArrayElement(arr: any[]): any {
    if (!Array.isArray(arr)) throw new Error('Input should be an array');
    return arr.length >= 2
      ? arr[Math.round(Math.random() * (arr.length - 1))]
      : arr[0];
  }
}
