import { readFileSync } from 'fs';

import { Provider } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import { ISongsMap } from './character-map.interface';

export const RADIO_LIB = Symbol('RADIO_LIB');

export const radioLibraryProvider: Provider = {
  provide: RADIO_LIB,
  useFactory: (): ISongsMap => {
    const radioLibFile =
      process.env.RADIO_LIB_JSON || './data/RadioLibrary.json';
    try {
      return JSON.parse(readFileSync(radioLibFile, 'utf-8')).map;
    } catch (err) {
      Logger.error(
        `An error occurred while reading radio library ${radioLibFile}`,
      );
      Logger.error(err);
      process.exit(1);
    }
  },
};
