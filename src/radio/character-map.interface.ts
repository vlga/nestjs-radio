import { IArtist } from './artist.interface';

export interface ICharacterMap {
  [key: string]: IArtist[];
}
export interface ISongsMap {
  firstCharacterMap: ICharacterMap;
}
