## Radio (playlist generator) using NestJS example

### 0. Requirements
See [REQUIREMENTS](./REQUIREMENTS.md)

### 1. Data preparation
```bash
npm run db:download
```
This command downloads and build character map for playlist generator
This script put data to file `data/RadioLibrary.json`.

### 2. App testing
There are 2 types of test:
* Unit - `*spec.ts` inside src and scripts
* E2E - API tests with supertest inside `test/` directory

All tests could be run with npm command
```bash
npm run test
```

### 3. App API
There are 2 endpoints:
* `/app/health` - app health check
* `/radio/playlist/:length` - playlist generator

With using NestJS OpenAPI docs could be generated easily (see [NestJS / Swagger](https://docs.nestjs.com/recipes/swagger))

By default db file `data/RadioLibrary.json` is used, but it could be changed with ENV variable `RADIO_LIB_JSON`

### 4. ToDo

#### Bug fixes and test coverage, other features
* improve test coverage
* fix corner cases (possible songs duplicates, short playlists)
* performance improvements, for example, add caching
* bonus features

#### Infrastructure
* add 2 stage Dockerfile
* add pre-hooks

