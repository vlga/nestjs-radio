export function buildMap(radioLibrary) {
    const songs = radioLibrary
    .map(a => a.Song)
    .reduce((prev, cur) => ([...prev, ...cur]), [])
    .filter(s => s.name);

    const firstCharacterMap = {};
    for (const s of songs) {
        const name = s.name.toString();
        const firstChar = name[0].toLowerCase();
        if (!firstCharacterMap[firstChar]) firstCharacterMap[firstChar] = [];

        firstCharacterMap[firstChar].push(s);
    }

    return {firstCharacterMap};
}
