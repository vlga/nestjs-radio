import * as fastXmlParser from 'fast-xml-parser';
import { readFileSync } from 'fs';

import { buildMap } from './build-map';

// We have to to use wrapper around fast-xml-parser
// `npx xml2js` doesn't support passing arrayMode: true

try {

    // Lib doesn't support streams
    // https://github.com/NaturalIntelligence/fast-xml-parser/issues/193

    const xml = readFileSync('/dev/stdin').toString();

    const parserOptions = {
        attributeNamePrefix: '',
        ignoreAttributes: false,
        allowBooleanAttributes: false,
        parseNodeValue: true,
        parseAttributeValue: true,
        trimValues: true,
        parseTrueNumberOnly: false,
        arrayMode: true,
    };

    const json = fastXmlParser.parse(xml, parserOptions, true);

    process.stdout.write(JSON.stringify({
        lib: json.Library[0].Artist,
        map: buildMap(json.Library[0].Artist),
    }));

} catch (err) {
    process.stderr.write('Can\'t convert XML to JSON \n');
    if (err) {
        process.stderr.write(err ? err.message : 'error undefined');
        process.stderr.write(err ? err.stack : 'error undefined');
    }
    process.exit(1);
}
