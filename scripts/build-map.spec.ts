import { buildMap } from './build-map';

const libMock = [
    {
        name: '10,000 Maniacs',
        Song: [
            {
                name: 'Peace Train',
                id: 3276,
                duration: 206706,
            },
            {
                name: 'Candy Everybody Wants (unplugged)',
                id: 3332,
                duration: 199575,
            },
        ],
        },
        {
            name: '2 Unlimited',
            Song: [
                {
                    name: 'Get Ready For This (Orchestral Mix)',
                    id: 3731,
                    duration: 327471,
                },
                {
                    name: 'Do What\'s Good For Me',
                    id: 3791,
                    duration: 136829,
                },
            ],
        },
];

describe('Test build_map function', () => {
    it('Should correctly build map', () => {
        const map = buildMap(libMock);
        expect(map.firstCharacterMap['p'].length).toBe(1);
    });

    it('Should correctly handle number as names', () => {
        const map = buildMap([{name: 'band name', Song: [{name: 123}]}]);
        expect(map.firstCharacterMap[1].length).toBe(1);
    });
});
